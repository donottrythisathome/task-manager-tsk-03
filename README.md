# TASK MANAGER 

Console application for task list.

# DEVELOPER INFO 

NAME: Ushakov Ivan  
E-MAIL: iushakov@tsconsulting.com

# SOFTWARE 

* JDK 15.0.1  
* OS Windows  

# HARDWARE 

* RAM 16Gb  
* CPU i5
* HDD 128Gb  

# RUN PROGRAM 

    java -jar ./task-manager-tsk-03.jar

# SCREENSHOTS 

https://drive.google.com/drive/folders/1YqxV7r-LgjQ9O9HXj-Wx4t2NcdRESdrT?usp=sharing
